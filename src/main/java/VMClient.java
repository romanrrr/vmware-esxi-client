import com.vmware.vim25.*;
import com.vmware.vim25.mo.*;
import org.apache.commons.cli.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * Created by q on 29.07.2015.
 */
public class VMClient {

    ServiceInstance si;

    public void connect(String host, String login, String password) {
        try {
            si = new ServiceInstance(new URL("https://" + host + "/sdk"), login, password, true);
            System.out.println("Connected");
        } catch (RemoteException e) {
            System.out.println("Couldn't connect to server!");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void Disconnect() {
        si.getServerConnection().logout();
        System.out.println("Disconnected");
    }

    public void showVMs() throws RemoteException {

        Folder rootFolder = si.getRootFolder();
        ManagedEntity[] mes = new InventoryNavigator(rootFolder).searchManagedEntities("VirtualMachine");

        if (mes != null && mes.length != 0) {
            int vmCount = mes.length;
            for (int i = 0; i < vmCount; i++) {
                VirtualMachine vm = (VirtualMachine) mes[i];
                VirtualMachineConfigInfo vminfo = vm.getConfig();
                System.out.println(i + 1 + ". " + vminfo.getName() + " | " + vminfo.getGuestFullName());
            }
        }
    }

    public void destroyVM(String name) throws RemoteException {

        Folder rootFolder = si.getRootFolder();
        ManagedEntity[] mes = new InventoryNavigator(rootFolder).searchManagedEntities("VirtualMachine");

        if (mes != null && mes.length != 0) {
            int vmCount = mes.length;
            for (int i = 0; i < vmCount; i++) {
                VirtualMachine vm = (VirtualMachine) mes[i];
                if (vm.getName().equals(name)) {
                    vm.destroy_Task();
                    System.out.println("Success");
                    return;
                }
            }
            System.out.println("The VM not found");
        }
    }

    public void create(String name, int numCpu, long memMB, String guestId, String datastoreName, long diskSizeKB) throws RemoteException {
        Folder rootFolder = si.getRootFolder();

        String datacenterName = "ha-datacenter";
        String diskMode = "persistent";
        Datacenter dc = (Datacenter) new InventoryNavigator(rootFolder).searchManagedEntity("Datacenter", datacenterName);

        ResourcePool rp = (ResourcePool) new InventoryNavigator(dc).searchManagedEntities("ResourcePool")[0];
        Folder vmFolder = dc.getVmFolder();

        VirtualMachineConfigSpec vmSpec = new VirtualMachineConfigSpec();
        vmSpec.setName(name);
        vmSpec.setMemoryMB(memMB);
        vmSpec.setNumCPUs(numCpu);
        vmSpec.setGuestId(guestId);

        int cKey = 1000;

        VirtualDeviceConfigSpec diskSpec = createDiskSpec(datastoreName, cKey, diskSizeKB, diskMode, 4);
        VirtualDeviceConfigSpec scsiSpec = createScsiSpec(cKey);
        vmSpec.setDeviceChange(new VirtualDeviceConfigSpec[]{scsiSpec, diskSpec});

        VirtualMachineFileInfo vmfi = new VirtualMachineFileInfo();
        vmfi.setVmPathName("[" + datastoreName + "]");
        vmSpec.setFiles(vmfi);

        Task task = vmFolder.createVM_Task(vmSpec, rp, null);
        String result = null;
        try {
            result = task.waitForTask();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("ERROR!! InterruptedException occured!");
        }
        if (!result.equals(Task.SUCCESS)) {
            System.out.println("VM not created " + result);
        } else {
            System.out.println("Success");
        }
    }

    public static VirtualDeviceConfigSpec createDiskSpec(String dsName, int cKey, long diskSizeKB, String diskMode, int unitNumber) {
        VirtualDeviceConfigSpec diskSpec = new VirtualDeviceConfigSpec();
        diskSpec.setOperation(VirtualDeviceConfigSpecOperation.add);
        diskSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.create);
        VirtualDisk vd = new VirtualDisk();
        vd.setCapacityInKB(diskSizeKB);
        diskSpec.setDevice(vd);
        vd.setKey(0);
        vd.setUnitNumber(unitNumber);
        vd.setControllerKey(cKey);
        VirtualDiskFlatVer2BackingInfo diskfileBacking = new VirtualDiskFlatVer2BackingInfo();
        String fileName = "[" + dsName + "]";
        diskfileBacking.setFileName(fileName);
        diskfileBacking.setDiskMode(diskMode);
        diskfileBacking.setThinProvisioned(true);
        vd.setBacking(diskfileBacking);
        return diskSpec;
    }

    static VirtualDeviceConfigSpec createScsiSpec(int cKey) {
        VirtualDeviceConfigSpec scsiSpec =
                new VirtualDeviceConfigSpec();
        scsiSpec.setOperation(VirtualDeviceConfigSpecOperation.add);
        VirtualLsiLogicController scsiCtrl =
                new VirtualLsiLogicController();
        scsiCtrl.setKey(cKey);
        scsiCtrl.setBusNumber(0);
        scsiCtrl.setSharedBus(VirtualSCSISharing.noSharing);
        scsiSpec.setDevice(scsiCtrl);
        return scsiSpec;
    }

    public static boolean contains(String test) {
        for (VirtualMachineGuestOsIdentifier c : VirtualMachineGuestOsIdentifier.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws RemoteException {
        VMClient vmClient = new VMClient();

        Options options = new Options();
        options.addOption("h", true, "Host");
        options.addOption("u", true, "User name");
        options.addOption("p", true, "Password");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (!cmd.hasOption('h') || !cmd.hasOption('u') || !cmd.hasOption('p')) {
                throw new ParseException("All argument is required");
            }
            vmClient.connect(cmd.getOptionValue("h"), cmd.getOptionValue("u"), cmd.getOptionValue("p"));
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            System.out.println("Run example: java VMClient -h 192.168.1.14 -u username -p password");
            return;
        }

        Scanner sc = new Scanner(System.in);
        String command;
        while (true) {
            System.out.print(">");
            command = sc.nextLine();
            if (command.equals("show")) {
                vmClient.showVMs();
            } else if (command.equals("q")) {
                break;
            } else if (command.equals("help")) {
                System.out.println("show - show all virtual machines\nq - exit\ncreate - create virtual machine\nremove - remove VM");
            } else if (command.equals("create")) {
                String name, guestId, storageName;
                int cpu;
                long memMB, diskKB;
                System.out.print("Enter the VM name: ");
                name = sc.nextLine();
                while (true) {
                    System.out.print("Enter the guest system id: ");
                    guestId = sc.nextLine();
                    if (contains(guestId)) {
                        break;
                    } else {
                        System.out.print("Guest ID must be one of : [");
                        for (VirtualMachineGuestOsIdentifier c : VirtualMachineGuestOsIdentifier.values()) {
                            System.out.print(c.toString() + ", ");
                        }
                        System.out.println("]");
                    }
                }
                System.out.print("Enter count of CPUs: ");
                cpu = Integer.parseInt(sc.nextLine());
                System.out.print("Enter memory size in MB: ");
                memMB = Long.parseLong(sc.nextLine());
                System.out.print("Enter the storage name: ");
                storageName = sc.nextLine();
                System.out.print("Enter disk size in KB: ");
                diskKB = Long.parseLong(sc.nextLine());
                vmClient.create(name, cpu, memMB, guestId, storageName, diskKB);
            } else if(command.equals("remove")){
                System.out.print("Enter the name of a removable VM: ");
                vmClient.destroyVM(sc.nextLine());
            } else {
                System.out.print("Command doesn't exist. Enter 'help'");
            }
        }
        vmClient.Disconnect();
    }
}
